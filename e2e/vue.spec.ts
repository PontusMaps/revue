import { test, expect } from '@playwright/test';

// See here how to get started:
// https://playwright.dev/docs/intro
test('visits the app root url', async ({ page }) => {
  await page.goto('/');
})

test('press button',async ({ page }) => {
  expect(await page.getByRole('button', { name: 'ReButton' })).toBeVisible
})

test('log in',async ({ page }) => {
  await page.goto('/');
  await page.getByLabel('Din mail').fill('test@test.com')
  await page.getByLabel('Ditt lösenord').fill('Test123')
  await page.getByRole('button', { name: 'Skicka'}).click

  await expect(await page.getByRole('heading', {name: 'Irrelevant!!!'}).first()).toBeVisible
})
